package com.example.kalkulator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var edtInput1: EditText
    private lateinit var edtInput2: EditText
    private lateinit var txtHasil: TextView
    private lateinit var btnTambah: Button
    private lateinit var btnKurang: Button
    private lateinit var btnKali: Button
    private lateinit var btnBagi: Button
    private lateinit var btnClear: Button

    companion object{
        private const val STATE_RESULT = "state_result"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //initial variable
        edtInput1 = findViewById(R.id.edtInput1)
        edtInput2 = findViewById(R.id.edtInput2)
        txtHasil = findViewById(R.id.txtHasil)
        btnTambah = findViewById(R.id.btnTambah)
        btnKurang = findViewById(R.id.btnKurang)
        btnKali = findViewById(R.id.btnKali)
        btnBagi = findViewById(R.id.btnBagi)
        btnClear = findViewById(R.id.btnClear)
        txtHasil = findViewById(R.id.txtHasil)

        if (savedInstanceState != null){
            val result = savedInstanceState.getString(STATE_RESULT) as String
            txtHasil.text = result
        }

        //action tambah
        btnTambah.setOnClickListener {
            if (!validate()){
                val inputOne = edtInput1.text.toString().trim()
                val inputTwo = edtInput2.text.toString().trim()
                val result = inputOne.toDouble() + inputTwo.toDouble()
                txtHasil.text = result.toString()
            }
        }
        //action kurang
        btnKurang.setOnClickListener {
            if (!validate()){
                val inputOne = edtInput1.text.toString().trim()
                val inputTwo = edtInput2.text.toString().trim()
                val result =  inputOne.toDouble() - inputTwo.toDouble()
                txtHasil.text = result.toString()
            }
        }
        //action kali
        btnKali.setOnClickListener {
            if (!validate()){
                val inputOne = edtInput1.text.toString().trim()
                val inputTwo = edtInput2.text.toString().trim()
                val result =  inputOne.toDouble() * inputTwo.toDouble()
                txtHasil.text = result.toString()
            }
        }
        //action bagi
        btnBagi.setOnClickListener {
            if (!validate()){
                val inputOne = edtInput1.text.toString().trim()
                val inputTwo = edtInput2.text.toString().trim()
                val result =  inputOne.toDouble() / inputTwo.toDouble()
                txtHasil.text = result.toString()
            }
        }
        //clear edit text
        btnClear.setOnClickListener {
            edtInput1.setText("")
            edtInput2.setText("")
            txtHasil.text = "0"
            edtInput1.requestFocus()

        }
    }

    private fun validate(): Boolean {
        var isEmptyField = false

        if (edtInput1.text.isEmpty()){
            isEmptyField = true
            edtInput1.error = "Field Tidak Boleh Kosong"
        }
        if (edtInput2.text.isEmpty()){
            isEmptyField = true
            edtInput2.error = "Field Tidak Boleh Kosong"
        }

        return isEmptyField
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(STATE_RESULT, txtHasil.text.toString())
    }
}